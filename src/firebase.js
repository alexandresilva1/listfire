import * as firebase from "firebase/app";

import "firebase/auth";
import "firebase/firestore";

firebase.initializeApp({
    apiKey: 'AIzaSyCoWjrwgH1uZ0Evv3s6WloyvnQ_GbkVYHg',
    authDomain: 'listing-beta.firebaseapp.com',
    projectId: 'listing-beta'
})

firebase.getCurrentUser = () => {
    return new Promise((resolve, reject) => {
        const unsubscribe = firebase.auth().onAuthStateChanged(user => {
            unsubscribe()
            resolve(user)
        }, reject);
    })
};

export default firebase
