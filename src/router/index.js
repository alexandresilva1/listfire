import Vue from 'vue'
import VueRouter from 'vue-router'
import firebase from '@/firebase'

import Home from '../components/Home.vue'
import Login from '../components/Login.vue'
import Register from '../components/Register.vue'
import Profile from '../components/Profile'
import Cards from '../components/Cards/Cards'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '*',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        requiresSignOut: true
      }
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: {
        requiresSignOut: true
      }
    },
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
      meta: {
        requiresAuth: true
      },
    },
    {
      path: '/cards',
      name: 'cards',
      component: Cards,
      meta: {
        requiresAuth: true
      },
    }
  ]
})

router.beforeEach( async (to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  const requiresSignOut = to.matched.some(record => record.meta.requiresSignOut)
  const currentUser = await firebase.getCurrentUser()

  if (requiresAuth && !currentUser)
    next('login')
  else if(requiresSignOut && currentUser) 
    next('/')
  else next()
})


export default router
