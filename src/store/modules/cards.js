import firebase from '@/firebase'

export default {
    state: {
        cards: []
    },
    getters: {
        getCards(state){
            return state.cards || []
        }
    },
    mutations: {
        setCards(state, { newIndex, oldIndex, doc, type }){
            if (type === 'added') {
                state.cards.splice(newIndex, 0, {id: doc.id ,...doc.data()})
            } else if (type === 'modified') {
                state.cards.splice(oldIndex, 1)
                state.cards.splice(newIndex, 0, {id: doc.id ,...doc.data()})
            } else if (type === 'removed') {
                state.cards.splice(oldIndex, 1)
            }            
        }
    },
    actions: {
        stateCards({ commit, rootState }){
            const uid = rootState.users.user.uid
            if(uid){
                const unsubscribe = firebase.firestore()
                    .collection('users').doc(uid).collection('cards')
                    .onSnapshot((snapshot) => {
                        snapshot.docChanges().forEach( (change) => {
                            const { newIndex, oldIndex, doc, type } = change
                            commit('setCards', { newIndex, oldIndex, doc, type })
                        })
                    }, () => unsubscribe())
            }
            
        },
        addCard({ rootState }, card){
            const uid = rootState.users.user.uid
            firebase.firestore()
                .collection('users').doc(uid).collection('cards').add(card)
                .then( docRef => {
                    console.log("Document written with ID: ", docRef.id);
                })
                .catch( error => {
                    console.error("Error adding document: ", error);
                });
        },
        editCard({ rootState }, cardInfo){
            const uid = rootState.users.user.uid
            const { id: idCard, ...card } = cardInfo
            firebase.firestore()
                .collection('users').doc(uid).collection('cards').doc(idCard)
                .update(card)
                .then(() => {
                    console.log("Document successfully updated!");
                })
                .catch((error) => {
                    console.error("Error updating document: ", error);
                });
        },
        deleteCard({ rootState }, idCard){
            const uid = rootState.users.user.uid
            firebase.firestore()
                .collection('users').doc(uid).collection('cards').doc(idCard)
                .delete().then(() => {
                    console.log("Document successfully deleted!");
                }).catch(error => {
                    console.error("Error removing document: ", error);
                })
        }
    }

}