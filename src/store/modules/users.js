import firebase from '@/firebase'

export default {
    namespaced: true,
    state: {
        user: null,
        status: null,
        error: null
    },
    getters: {
        status(state) {
            return state.status
        },
        user(state) {
            return state.user
        },
        error(state) {
            return state.error
        },
    },
    mutations: {
        setUser(state, payload) {
            state.user = payload
        },
        removeUser(state) {
            state.user = null
        },
        setStatus(state, payload) {
            state.status = payload
        },
        setError(state, payload) {
            state.error = payload
        }
    },
    actions: {
        createUser({ commit, dispatch }, payload){
            commit('setStatus', 'loading')
            firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
                .then((response) => {
                    const {uid, email} = response.user
                    commit('setUser', { uid, email })
                    dispatch('initUser')

                    commit('setStatus', 'success')
                    commit('setError', null)
                })
                .catch((error) => {
                    commit('setStatus', 'failure')
                    commit('setError', error.message)
                })
            commit('setStatus', 'loading')
        },
        signUser({commit}, payload){
            commit('setStatus', 'loading')
            firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
                .then((response) => {
                    const {uid, email} = response.user
                    commit('setUser', { uid, email })
                    commit('setStatus', 'success')
                    commit('setError', null)
                })
                .catch((error) => {
                    commit('setStatus', 'failure')
                    commit('setError', error.message)
                })
            commit('setStatus', 'finish')
        },
        signOut({ commit }){
            commit('setStatus', 'loading')
            firebase.auth().signOut()
                .then(() => {
                    commit('setUser', null)
                    commit('setStatus', 'success')
                    commit('setError', null)
                })
                .catch((error) => {
                    commit('setStatus', 'failure')
                    commit('setError', error.message)
                })
            commit('setStatus', 'finish')
        },
        initUser({ state }){
            firebase.firestore().collection("users").doc(state.user.uid).set({
                email: state.user.email,
                name: state.user.email.split('@')[0] || 'Altere seu nome'
            })
            .then(function() {
                console.log("Document successfully written!");
            })
            .catch(function(error) {
                console.error("Error writing document: ", error);
            });
        },
        authState({ commit }){
            const unsubscribe = firebase.auth().onAuthStateChanged(user => {
                unsubscribe()
                if(user){
                    const { uid, email} = user
                    commit('setUser', { uid, email })
                }else{
                    commit('setUser', false)
                }
            })
        }
    },
}